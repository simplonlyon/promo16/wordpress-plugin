<?php

/**
 * Plugin Name:     Simplongin
 * Plugin URI:      PLUGIN SITE HERE
 * Description:     PLUGIN DESCRIPTION HERE
 * Author:          YOUR NAME HERE
 * Author URI:      YOUR SITE HERE
 * Text Domain:     simplongin
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Simplongin
 */

add_action("admin_menu", "simplon_plugin_page");

function simplon_plugin_page()
{
    add_menu_page("Simplon Plugin", "Simplon Plugin", "manage_options", "simplon_plugin", "menu_display");

    add_action("admin_init", "register_plugin_settings");
}

function register_plugin_settings()
{
    register_setting("simplon_options", "admin_color");
}

function menu_display()
{
    require "admin-menu.php";
    echo "<p class=\"btn btn-primary\">coucou</p>";
}

add_action("admin_head", "admin_style");

function admin_style() {
    ?>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
        #wpadminbar, #adminmenuwrap *, #adminmenuwrap {
            background-color: <?php echo get_option("admin_color") ?>;
        }
    </style>
    

<?php
}