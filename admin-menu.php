<h1>Simplon plugin</h1>

<form action="options.php" method="post">
    <?php settings_fields('simplon_options'); ?>
    <?php do_settings_sections('simplon_options'); ?>
    <label for="admin_color">Admin Color</label>
    <input type="color" name="admin_color" id="admin_color" value="<?php echo esc_attr( get_option('admin_color') );?>">
    <?php submit_button() ?>
</form>